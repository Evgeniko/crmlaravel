@extends('layouts.main')
@section('title', 'Редактирование проекта')
@section('content')
<div class="row">
    <form role="form" method="POST">
        <input type="hidden" name="project_id" value="{{ $project->id }}">
        {{ csrf_field() }}
        <div class="col-xs-12 col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h1>Проект: {{ $project->name }}</h1>
                </div>
                <div class="col-xs-7">
                    <h3>Слайды</h3>
                    <div class="ibox-content">
                        <table class="table">
                            <thead>
                            <tr>
                                <th></th>
                                <th>#</th>
                                @can('if-admin')
                                    <th>Тип</th>
                                @endcan
                                <th>Комментарий</th>

                            </tr>
                            </thead>
                            <tbody id="slides">
                            @forelse($slides as $slide)
                                <tr>
                                    @can('if-admin')
                                        <td>
                                            <input type="checkbox" name="slides[{{$slide->id}}][checking]" value="1" {{ !$slide->checking?:'checked' }}>
                                        </td>
                                    @else
                                        <td>
                                            <input type="hidden" name="slides[{{$slide->id}}][checking]" value="{{ $slide->checking?1:0 }}">
                                        </td>
                                    @endcan
                                    <td>
                                        <input class="form-control" type="text" name="slides[{{ $slide->id }}][num]" value="{{ $slide->num }}" required />
                                    </td>
                                    @can('if-admin')
                                        <td>
                                            <select class="form-control m-b" name="slides[{{ $slide->id }}][type_id]">
                                                <option></option>
                                                @foreach($types as $type)
                                                    <option value="{{ $type->id }}" {{ $slide->type_id == $type->id?'selected':'' }}>
                                                        {{ $type->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                    @endcan
                                    <td>
                                        <input class="form-control" type="text" name="slides[{{ $slide->id }}][comment]" value="{{ $slide->comment }}">
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    Здесь пока пусто
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <a id="add-slide" class="btn btn-default">Добавить слайд</a>
                    </div>
                </div>
                <div class="col-xs-4 col-xs-offset-1">
                    <h3>Правки</h3>
                    <div class="ibox-content">
                        <table class="table">
                            <thead>
                            <tr>
                                <th></th>
                                <th>#</th>
                                <th>Комментарий</th>
                            </tr>
                            </thead>
                            <tbody id="edits">
                            @forelse($edits as $edit)
                                <tr>
                                    @can('if-admin')
                                        <td>
                                            <input type="checkbox" value="1" name="edits[{{$edit->id}}][checking]" {{ !$edit->checking?:'checked' }}>
                                        </td>
                                    @else
                                        <td>
                                            <input type="hidden" name="edits[{{$edit->id}}][checking]" value="{{ $edit->checking?1:0 }}">
                                        </td>
                                    @endcan
                                    <td>
                                        <input class="form-control" type="text" name="edits[{{ $edit->id }}][num]" value="{{ $edit->num }}" required/>
                                    </td>
                                    <td>
                                        <input class="form-control" type="text" name="edits[{{ $edit->id }}][comment]" value="{{ $edit->comment }}">
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    Здесь пока пусто
                                </tr>
                                <!--
                                <tr>
                                    <td>
                                        <input type="checkbox" value="1" name="newEdits[1][checking]" class="icheckbox_square-green">
                                    </td>
                                    <td>
                                        <input class="form-control" type="text" name="newEdits[1][num]">
                                    </td>
                                    <td>
                                        <input class="form-control" type="text" name="newEdits[1][comment]">
                                    </td>
                                </tr>
                                -->
                            @endforelse
                            </tbody>
                        </table>
                        <a  id="add-edit" class="btn btn-default">Добавить правки</a>
                    </div>
                </div>
                <div class="ibox-content">
                    <input type="submit" class="btn btn-primary pull-right" value="Сохранить">
                </div>
            </div>
        </div>

    </form>
</div>

@endsection