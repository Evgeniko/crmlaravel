@extends('layouts.main')
@section('title', 'Проекты')
@section('content')
    <form method="get" action="{{ url('/excel/export') }}">
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h4>Проекты</h4>
        <div class="ibox-tools">
            <div class="container">
            @can('if-admin')
                <input type="submit" class="btn btn-default" value="Экспорт в Excel">
            @endcan
            <a href="#" data-toggle="modal" data-target="#addProjectModal" class="btn btn-primary">Добавить проект</a>
            </div>
        </div>
    </div>
    <div class="ibox-content">
        <table class="table">
            <thead>
                <tr>
                    @can('if-admin')
                        <th></th>
                    @endcan
                    <th>#</th>
                    <th>Компания</th>
                    <th>Название</th>
                    <th>Последняя выгрузка</th>
                    <th>Слайдов</th>
                    <th>Статус</th>
                </tr>
            </thead>
            <tbody>
                @foreach($projects as $project)
                <tr class="{{ $project->changed ? 'changed' : '' }}">
                    @can('if-admin')
                        <td><input type="checkbox" name="ids[]" value="{{ $project->id }}"></td>
                    @endcan
                    <td>{{ $project->num }}</td>
                    <td>{{ $project->company? $project->company->name: '' }}</td>
                    <td>
                        <a href="{{ url("/project/edit/$project->id") }}">{{ $project->name }}</a>
                    </td>
                    <td>{{ $project->last_export?$project->last_export->diffForHumans() :'-' }}</td>
                    <td>{{ $project->slides->count() }}</td>
                    <td>
                        <span class="label status-label-{{ $project->status_id }}">
                            {{ $project->status->label }}
                        </span>
                    </td>
                    <td>
                        <a href="#" data-toggle="modal" data-target="#editProjectModal-{{ $project->id }}" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                    </td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{ $projects->render() }}
</div>
    </form>
<!-- Модаль для добавления проекта -->
<div class="modal inmodal in" id="addProjectModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <form role="form" method="POST" action="{{ url('/project/save') }}">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h4 class="modal-title">Добавить проект</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group"><label>#</label>
                        <input name="num" type="number" placeholder="" class="form-control" required>
                    </div>
                    <div class="form-group"><label>Компания</label>
                        <input name="company" type="text" placeholder="" class="form-control" required>
                    </div>
                    <div class="form-group"><label>Имя проекта</label>
                        <input name="name" type="text" placeholder="" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Закрыть</button>
                    <input type="submit" class="btn btn-primary" value="Добавить">
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Модаль для добавления проекта -->

<!-- Модали для редактирования проекта -->
@foreach($projects as $project)
    <div class="modal inmodal in" id="editProjectModal-{{ $project->id }}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <form role="form" method="POST" action="{{ url('/projects/update') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $project->id }}">
                    <div class="modal-header">
                        <h4 class="modal-title">Редактировать проект</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group"><label>#</label>
                            <input name="num" type="number" placeholder="" class="form-control" value="{{ $project->num }}" required>
                        </div>
                        <div class="form-group"><label>Компания</label>
                            <input name="company" type="text" placeholder="" class="form-control" value="{{ $project->company? $project->company->name: '' }}" required>
                        </div>
                        <div class="form-group"><label>Имя проекта</label>
                            <input name="name" type="text" placeholder="" class="form-control" value="{{ $project->name }}" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Закрыть</button>
                        <button type="submit" class="btn btn-primary">Изменить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endforeach
<!-- /Модали для редактирования проекта -->
@endsection
