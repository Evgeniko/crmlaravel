@extends('layouts.main')
@section('title', 'Просмотр пользователей сайта')
@section('content')
<div class="ibox-title">
    <h4>Пользователи</h4>
</div>
<div class="ibox-tools">
    <div class="container">
        <a href="#" data-toggle="modal" data-target="#addUserModal" class="btn btn-primary">Добавить пользователя</a>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    @foreach($users as $user)
        <div class="col-lg-4">
            <div class="contact-box">
                    <div class="col-sm-3">
                        <div class="text-center">
                            <img alt="image" class="img-circle m-t-xs img-responsive" src="{{ asset('/img/nefabrika.png') }}">
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <h3><strong>{{ $user->name }}</strong></h3>
                        <p class="{{ $user->role->id === 1? 'text-danger':'' }}">
                            <i class="fa fa-user"></i>
                            {{ $user->role->name }}
                        </p>
                        <p><i class="fa fa-envelope"></i>  {{ $user->email }}</p>
                    </div>
                    <div class="clearfix"></div>
                <div class="contact-box-footer">
                    <div class="m-t-xs btn-group">
                        <a href="#" data-toggle="modal" data-target="#editUserModal-{{$user->id}}" class="btn btn-xs btn-white"><i class="fa fa-pencil"></i> Редактировать </a>
                        <a href="{{ url("/user/delete/$user->id") }}" class="btn btn-xs btn-white"><i class="text-danger fa fa-trash"></i> Удалить</a>
                    </div>
                </div>
            </div>
        </div>

    @endforeach
</div>

<!-- Модаль для добавления пользователя -->
<div class="modal inmodal in" id="addUserModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <form role="form" method="POST" action="{{ url('/user/create') }}">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h4 class="modal-title">Добавить пользователя</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group"><label>Имя</label>
                        <input name="name" type="text" placeholder="Иван Иванов" class="form-control" required>
                    </div>
                    <div class="form-group"><label>Email</label>
                        <input name="email" type="email" placeholder="E-mail для входа" class="form-control" required>
                    </div>
                    <div class="form-group"><label>Пароль</label>
                        <input name="password" type="text" placeholder="Пароль для входа" class="form-control" required>
                    </div>
                    <div class="form-group"><label>Роль на сайте</label>
                        <select class="form-control m-b" name="role_id">
                            @foreach($roles as $role)
                                <option value="{{ $role->id }}" {{ $role->id == 2?'selected':'' }}>
                                    {{ $role->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Закрыть</button>
                    <input type="submit" class="btn btn-primary" value="Добавить">
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Модаль для добавления пользователя -->

<!-- Модали для редактирования пользователя -->
@foreach($users as $user)
    <div class="modal inmodal in" id="editUserModal-{{ $user->id }}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <form role="form" method="POST" action="{{ url("/user/update/$user->id") }}">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h4 class="modal-title">Редактировать пользователя</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group"><label>Имя</label>
                            <input name="name" type="text" placeholder="Иван Иванов" class="form-control" value="{{ $user->name }}" required>
                        </div>
                        <div class="form-group"><label>Email</label>
                            <input name="email" type="email" placeholder="E-mail для входа" class="form-control" value="{{ $user->email }}" required>
                        </div>
                        <div class="form-group"><label>Новый пароль</label>
                            <input name="password" type="text" placeholder="Новый пароль для входа" class="form-control" required>
                        </div>
                        <div class="form-group"><label>Роль на сайте</label>
                            <select class="form-control m-b" name="role_id">
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}" {{ $user->role_id == $role->id?'selected':'' }}>{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Закрыть</button>
                        <input type="submit" class="btn btn-primary" value="Сохранить">
                    </div>
                </form>
            </div>
        </div>
    </div>

@endforeach
<!-- /Модали для редактирования проекта -->
@stop