@if(session()->has('success'))
    <script type="text/javascript">
        window.onload = function () {
            toastr.success('{{ session('success') }}');
        }
    </script>
@endif

@if(session()->has('error'))
    <script type="text/javascript">
        window.onload = function () {
            toastr.error('{{ session('error') }}');
        }
    </script>
@endif

@if ($errors->any())
    @foreach ($errors->all() as $error)
        <script type="text/javascript">
            window.onload = function () {
                toastr.error('Ошибка:{{ $error }}');
            }
        </script>
    @endforeach
@endif