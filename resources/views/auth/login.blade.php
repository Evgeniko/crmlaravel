@extends('layouts.auth.main')
@section('content')
    <body class="signup-page">
    <div class="wrapper">
        <div class="header header-filter" style="background-image: url('{{ asset('img/cool.jpg') }}'); background-size: cover; background-position: top center;">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                        <div class="card card-signup">
                            <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="header header-info text-center">
                                    <h4>Войти</h4>
                                </div>
                                <div class="content">

                                    <div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
										<span class="input-group-addon">
											<i class="material-icons">email</i>
										</span>
                                        <input  id="email" name="email" type="text" class="form-control" placeholder="Email..."  value="{{ old('email') }}" required autofocus>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-group{{ $errors->has('password') ? ' has-error' : '' }}">
										<span class="input-group-addon">
											<i class="material-icons">lock_outline</i>
										</span>
                                        <input type="password" placeholder="Password..." class="form-control" name="password" required/>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="checkbox text-center">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                            Запомнить меня
                                        </label>
                                    </div>

                                </div>
                                <div class="footer text-center">
                                    <input type="submit" class="btn btn-simple btn-info btn-lg" value="Войти">
                                </div>
                                <!--<div class="text-center">
                                    <a class="btn btn-simple" href="{{ route('password.request') }}">
                                        Забыли пароль?
                                    </a>
                                </div>-->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer">
                <div class="container">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                    Подвал
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright pull-right">
                        &copy; 2017, made with <i class="fa fa-heart heart"></i> by <a href="http://nefabrika.ru/" target="_blank">neFabrika</a>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    </body>
@endsection
