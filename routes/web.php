<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth')->group(function (){
    Route::get('/', 'ProjectController@show');
//    Route::get('/projects', 'ProjectController@show');
    Route::get('/project/edit/{id}', 'ProjectController@showEditForm');
    Route::post('/project/edit/{id}', 'ProjectController@edit');
    Route::post('/project/save', 'ProjectController@save');
    Route::post('/projects/update', 'ProjectController@update');
    Route::get('/excel/export', 'ExcelController@export');

    Route::get('/users', 'UsersController@show');
    Route::post('/user/create', 'UsersController@create');
    Route::post('/user/update/{id}', 'UsersController@update');
    Route::get('/user/delete/{id}', 'UsersController@delete');
});


Auth::routes();

Route::get('/register', function (){
    return 'Упс:)';
});

//Route::get('/home', 'HomeController@index')->name('home');
