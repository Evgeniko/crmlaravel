<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            'name' => 'Тестовая компания'
        ]);

        for ($i = 0; $i < 300; $i++){

            DB::table('projects')->insert([
                'num' => rand(11, 9999),
                'name' => str_random(20),
                'company_id' => 1,
                'changed' => true
            ]);
        }

        for ($i = 0; $i < 30000; $i++){
            DB::table('slides')->insert([
                'project_id' => rand(1, 1000),
                'num' => rand(1, 90),
                'type_id' => rand(1, 5),
                'comment' => str_random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
            DB::table('edits')->insert([
                'project_id' => rand(1, 1000),
                'num' => rand(1, 90),
                'comment' => str_random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
    }
}
