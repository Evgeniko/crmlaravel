<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            [
                'name' => 'Дмитрий Илюхин',
                'email' => 'admin@admin.ru',
                'password' => bcrypt('123321'),
                'main' => true,
                'role_id' => 1,
            ],
            [
                'name' => 'Тестовый модератор',
                'email' => 'moder@moder.ru',
                'password' => bcrypt('123321'),
                'main' => false,
                'role_id' => 2,
            ]
        ]);

        DB::table('roles')->insert([
            ['name' => 'Администратор'],
            ['name' => 'Модератор'],
        ]);

        $this->call(StatusesTableSeeder::class);
        $this->call(TypesTableSeeder::class);
        //$this->call(TestDataSeeder::class);
    }
}
