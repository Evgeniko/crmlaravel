$(document).ready(function () {
    var slides_count = 2;
    var edits_count = 2;
    $('#add-slide').click(function () {
        $('#slides').append(
            '<tr>'+
            '<td>' +
            '<input type="checkbox" name="newSlides['+slides_count+'][checking]" value="1" class="icheckbox_square-green">'+
            '</td>'+
            '<td>'+
            '<input class="form-control" type="text" name="newSlides['+slides_count+'][num]" required>'+
            '</td>'+
            '<td>'+
            '<select class="form-control m-b" name="newSlides['+slides_count+'][type_id]">'+
            '<option selected></option>'+
            '<option value="1">Анимация</option>'+
            '<option value="2">Обычный</option>'+
            '<option value="3">Интерактивный</option>'+
            '<option value="4">Дубль</option>'+
            '<option value="5">Дубль+Правки</option>'+
            '</select>'+
            '</td>'+
            '<td>'+
            '<input class="form-control" type="text" name="newSlides['+slides_count+'][comment]">'+
            '</td>'+
            '</tr>'
        );
        slides_count++;
    });

    $('#add-edit').click(function () {
        $('#edits').append(
            '<tr>'+
            '<td>' +
            '<input type="checkbox" name="newEdits['+edits_count+'][checking]"  value="1" class="icheckbox_square-green">'+
            '</td>'+
            '<td>'+
            '<input class="form-control" type="text" name="newEdits['+edits_count+'][num]" required>'+
            '</td>'+
            '<td>'+
            '<input class="form-control" type="text" name="newEdits['+edits_count+'][comment]">'+
            '</td>'+
            '</tr>'
        );
        edits_count++;
    });

});