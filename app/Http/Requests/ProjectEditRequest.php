<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slides.*.num' => 'required|max:255',
            'slides.*.comment' => 'max:255',
            'edits.*.num' => 'required|max:255',
            'edits.*.comment' => 'max:255',
        ];
    }
}