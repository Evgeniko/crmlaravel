<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function export(Request $request)
    {
        $this->authorize('if-admin');

        if ($ids = $request->ids) {
            $projects = Project::with('slides.type', 'edits', 'status', 'company')->whereIn('id', $ids)->where('changed', 1)->get();
            $excel = Excel::create('CRM');

            $excel->sheet('Лист', function ($sheet) use ($projects) {
                $row = 1;

                foreach ($projects as $project) {
                    $row++;
                    //$sheet->setWidth('A', 4);
                    $sheet->mergeCells("A$row:D$row");

                    $sheet->row($row, function ($row) {
                        $row->setFontSize(18);
                    });
                    $sheet->row($row++, [$project->company->name . ' - ' . $project->name]);

                    $sheet->mergeCells("A$row:D$row");
                    $sheet->row($row, function ($row) {
                        $row->setFontSize(13);
                    });
                    $sheet->row($row, function ($row) {
                        $row->setBackground('#79d4e7');
                    });
                    $sheet->row($row++, ['Слайды']);

                    foreach ($project->slides as $slide) {
                        if($slide->created_at < $project->last_export){
                            continue;
                        }
                        $sheet->row($row++, [
                            $slide->num,
                            ($slide->type) ? $slide->type->name : '',
                            '         ',
                            $slide->comment,
                        ]);
                    }

                    $sheet->mergeCells("A$row:D$row");
                    $sheet->row($row, function ($row) {
                        $row->setFontSize(13);
                    });
                    $sheet->row($row, function ($row) {
                        $row->setBackground('#eebfdd');
                    });
                    $sheet->row($row++, ['Правки']);

                    //$sheet->setWidth('A', 1);
                    foreach ($project->edits as $edit) {
                        if($edit->created_at < $project->last_export){
                            continue;
                        }
                        $sheet->row($row++, [
                            $edit->num,
                            'правки',
                            '',
                            $edit->comment
                        ]);
                    }

                    $sheet->row($row, function ($row) {
                        $row->setFontSize(13);
                    });
                    $sheet->row($row++, [
                        '',
                        'Итого'
                    ]);

                    $project->last_export = Carbon::now();
                    $project->save();
                }
            });

            $excel->download('xls');
        }else{
            Session::flash('error', 'Не выбраны проекты для выгрузки');
            return back();
        }
    }

}
