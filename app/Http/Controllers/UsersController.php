<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:if-admin, App\Policies\RolePolicy');
    }

    /**
     * Show all users
     */
    public function show()
    {
        //$this->authorize('if-admin');
        $roles = Role::all();
        $users = User::with('role')->paginate();

        return view('users.show', compact('users', 'roles'));
    }

    /**
     * Create new user
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'email' => 'unique:users'
        ]);
        $request = $request->toArray();
        $request['password'] = bcrypt($request['password']);
        User::create($request);

        return back()->with('success', 'Новый пользователь успешно создан');
    }

    /**
     * Update user by id
     *
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        $request = $request->toArray();
        $user = User::find($id);
        if ($user->main){
            return back()->with('error', 'Невозможно редактировать главного пользователя');
        }
        $request['password'] = bcrypt($request['password']);
        $user->update($request);
        return back()->with('success', 'Пользователь успешно отредактирован');
    }

    /**
     * Delete user by id
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $user = User::find($id);
        if ($user->main){
            return back()->with('error', 'Невозможно удалить главного пользователя');
        }
        $user->delete();

        return back()->with('success', 'Пользователь успешно удалён');
    }

}
