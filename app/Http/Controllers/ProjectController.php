<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectEditRequest;
use App\Models\Company;
use App\Models\Edit;
use App\Models\Project;
use App\Models\Slide;
use App\Models\Type;
use App\Services\ProjectStatusService;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request, Project $project)
    {
        $company = Company::firstOrCreate(['name' => $request->company]);
        $project->num = $request->num;
        $project->name = $request->name;
        $project->company_id = $company->id;
        $project->save();
        return back()->with('success', 'Новый проект был успешно добавлен');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectStatusService $projectStatus)
    {
        $projects = Project::with('slides', 'edits', 'status', 'company')->orderBy('last_export')->paginate(25);
        $projectStatus->setChanged($projects);
        return view('projects.show', compact('projects'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function showEditForm(Request $request, $id)
    {
        $project = Project::find($id);
        $types = Type::all();
        $slides = $project->slides;
        $edits = $project->edits;
        return view('projects.edit', compact('project', 'types', 'slides', 'edits'));
    }

    /**
     * Edit resourses
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit(ProjectEditRequest $request, ProjectStatusService $projectStatus)
    {
        if ($request->slides){
            foreach ($request->slides as $key => $slide_request){
                $slide = Slide::find($key);
                array_key_exists('checking', $slide_request)?:$slide_request['checking'] = 0;
                $slide->update($slide_request);
            }
        }

        if ($request->edits){
            foreach ($request->edits as $key => $edit_request){
                $edit =  Edit::find($key);
                array_key_exists('checking', $edit_request)?:$edit_request['checking'] = 0;
                $edit->update($edit_request);
            }
        }

        if ($request->newSlides){
            foreach ($request->newSlides as $newSlide_request){
                $newSlide_request['project_id'] = $request->project_id;
                Slide::create($newSlide_request);
            }
        }

        if ($request->newEdits){
            foreach ($request->newEdits as $newEdit_request){
                $newEdit_request['project_id'] = $request->project_id;
                Edit::create($newEdit_request);
            }
        }
        //Перезапись статуса проекта на основе новых сохранённых данных
        $projectStatus->get($request);
        //$request->session()->flash();
        return redirect('/')->with('success', 'Проект успешно отредактирован');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $company = Company::updateOrCreate(['name' => $request->company]);
        unset($request['company']);
        $request['company_id'] = $company->id;
        $project = Project::find($request->id);
        $project->update($request->toArray());
        return back()->with('success', 'Проект был успешно отредактирован');
    }

}