<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guarded = ['id'];

    public function projects()
    {
        return $this->hasMany('App\Models\Project');
    }
}
