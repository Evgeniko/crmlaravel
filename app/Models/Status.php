<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'statuses';

    protected $guarded = ['id'];

    public function projects()
    {
        return $this->hasMany('App\Models\Project' , 'status_id');
    }
}
