<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $guarded = ['id'];

    protected $dates = [
        'created_at',
        'updated_at',
        'last_export',
    ];

    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }

    public function edits()
    {
        return $this->hasMany('App\Models\Edit');
    }

    public function slides()
    {
        return $this->hasMany('App\Models\Slide', 'project_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }
}
