<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Check user role is admin
     *
     * @param User $user
     * @return bool
     */
    public function checkForAdmin(User $user)
    {
        return $user->role_id === 1;
    }

}
