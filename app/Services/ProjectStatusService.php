<?php
namespace App\Services;


use App\Models\Edit;
use App\Models\Project;
use App\Models\Slide;

class ProjectStatusService
{

    /**
     * Get project status and update status_id in Database
     *
     * @param $project
     * @param $request
     * @return int
     */
    public function get($request)
    {
        $project = Project::find($request->project_id);
        $slides = Slide::where('project_id', $request->project_id)->get();
        $edits = Edit::where('project_id', $request->project_id)->get();


        $slides_done = $this->checkDone($slides);
        $slides_during = $this->checkDuring($slides);
        $slide_status = $this->getStatus($slides_done, $slides_during);

        $edits_done = $this->checkDone($edits);
        $edits_during = $this->checkDuring($edits);
        $edit_status = $this->getStatus($edits_done, $edits_during);
        //dd($slide_status."-".$edit_status);
        if ($slide_status == 3 && $edit_status == 3){
            $project->update(['status_id' => 3]);
        }elseif (($slide_status == 3 || $edit_status == 3) && ($edit_status == 123 || $slide_status == 123)){
            $project->update(['status_id' => 3]);
        }elseif ($slide_status == 123 && $edit_status == 123){
            $project->update(['status_id' => 1]);
        }elseif ($slide_status == 1 && $edit_status == 1){
            $project->update(['status_id' => 1]);
        }elseif (($slide_status == 1 || $edit_status == 1) && ($edit_status == 123 || $slide_status == 123)){
            $project->update(['status_id' => 1]);
        }else{
            $project->update(['status_id' => 2]);
        }

        return true;
    }

    public function setChanged($projects)
    {
        foreach ($projects as $project){
            $changed = $this->checkChanged($project);
            $project->changed = $changed;
            $project->save();
        }

    }

    public function checkChanged($project){
        foreach($project->slides as $slide){
            if ($slide->created_at > $project->last_export){
                return true;
            };
        }
        foreach ($project->edits as $edit){
            if ($edit->created_at > $project->last_export){
                return true;
            };
        }

        return false;
    }
    /**
     * Return true if all checking true
     *
     * @param $params
     * @return string
     */
    private function checkDone($params)
    {
        if ($params->isEmpty()){
            return 123;//false;
        }
        foreach ($params as $param) {
            if (!$param->checking) {
                return  false;
            }
        }
        return true;

    }

    /**
     * Return true if all checking false
     *
     * @param $params
     * @return bool
     */
    private function checkDuring($params)
    {
        if ($params->isEmpty()){
            return 123;//true;
        }
        foreach ($params as $param) {
            if ($param->checking) {
                return false;
            }
        }
        return true;
    }

    /**
     * Return status
     *
     * @param $done
     * @param $during
     * @return int
     */
    private function getStatus($done, $during)
    {
        if ($done == 123 && $during == 123){
            return 123;
        }
        if ($done && !$during){
            return 3;
        }
        if (!$done && $during){
            return 1;
        }
        if (!$done && !$during){
            return 2;
        }
    }

}